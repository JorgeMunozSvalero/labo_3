let BASE_URL = "https://jorgemypasssaver.azurewebsites.net"

function calculateIdNewSite() {
    let length = getDataAmount()
    let idNewSite = ++length;
    return idNewSite;
}

function redirectToData() {
    window.location.href = "/index.html"
}

window.onload = function(e) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    if (urlParams.has('id')) {
        value = urlParams.get('id')
        fetch("https://jorgemypasssaver.azurewebsites.net/UsersData/" + value)
            .then(response => response.json())
            .then(data => {
                document.getElementById('siteInput').value = data.site;
                document.getElementById('userInput').value = data.user;
                document.getElementById('passwordInput').value = data.password;
                document.getElementById('categoryID_Input').value = data.categoryId;
            })
        document.getElementById('success').onclick = function() {
            postSite(value);
        }
    } else {
        document.getElementById('success').onclick = addNewSite;
    }

}

function postSite(id) {
    let id_int = parseInt(id)
    let varSite = document.getElementById('siteInput').value;
    let varUser = document.getElementById('userInput').value;
    let varPass = document.getElementById('passwordInput').value;
    let varCatID = document.getElementById('categoryID_Input').value;
    let id_cat = parseInt(varCatID)
    let data = { "id": id_int, "site": varSite, "user": varUser, "password": varPass, "categoryId": id_cat }
    let url = "https://jorgemypasssaver.azurewebsites.net/UsersData/" + id_int
    fetch(url, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            res.json();
            redirectToData();
        })
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
}

function getInput(name) {
    return document.getElementById(`${name}`).value
}

function getDataAmount() {
    let count = 0
    fetch("https://jorgemypasssaver.azurewebsites.net/UsersData/", {
            method: 'GET'
        })
        .then(response => response.json())
        .then(data => {
            count = data.length
            count++;
            return count
        })
        .catch(function(error) {
            console.log('Hubo un problema con getDataAmnt petición Fetch:' + error.message);
        });
}

function addNewSite() {

    let url = "https://jorgemypasssaver.azurewebsites.net/UsersData/"
    let data = { "id": 0, "site": getInput("siteInput"), "user": getInput("userInput"), "password": getInput("passwordInput") }
    fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(res => {
            res.text();
            redirectToData();
        })
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
}