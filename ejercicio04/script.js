let BASE_URL = "https://jorgemypasssaver.azurewebsites.net"
let SECURE_DATA = ""

window.onload = function(e) {
    loadAllCategories();
    loadAllData();
}

function addCategory() {
    document.getElementById("createCategory").classList.toggle("d-none");
}

function cancelCategory() {
    document.getElementById("createCategory").classList.toggle("d-none");
}

function addSite() {
    window.location.href = "/sites.html"
}

function calculateIdNewCategory() {
    let length = document.getElementsByClassName("list-group-item list-group-item-action list-group-item-light text_white hover_black").length
    let idNewCategory = ++length;
    return idNewCategory;
}

function getNameInput(name) {
    return document.getElementById(name).value
}

function copy() {
    document.execCommand("copy");
    alert("item copiado")
}

function addNewCategory() {

    let url = "https://jorgemypasssaver.azurewebsites.net/Categories"
    let data = { "id": calculateIdNewCategory(), "nombre": getNameInput("categoryTitleInput") }
    fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(res => {
            res.text();
            location.reload()
        })
        .catch(error => console.error('Error:' + error))
        .then(response => console.log('Success:' + response));

}

function deleteCategory(id) {

    fetch('https://jorgemypasssaver.azurewebsites.net/Categories/' + id, {
            method: 'DELETE',
        })
        .then(res => {
            res.text();
            location.reload();
            loadAllData();
        })
        .catch(error => console.error('Error:' + error))
        .then(response => console.log('Success:' + response));

}

function deleteSite(id) {

    fetch('https://jorgemypasssaver.azurewebsites.net/UsersData/' + id, {
            method: 'DELETE',
        })
        .then(res => {
            res.text();
            loadAllData();
        })
        .catch(error => console.error('Error:' + error))
        .then(response => console.log('Success:' + response));
}

function loadAllCategories() {

    fetch(BASE_URL + '/categories')
        .then(response => response.json())
        .then(data => {
            document.getElementById("categories").innerHTML = ''
            data.forEach(categories => {
                document.getElementById("categories").innerHTML +=
                    '<div class="wrapperCategories">' +
                    '<li id=' + 'cat-' + categories.id + ' style="cursor:pointer;" class="list-group-item list-group-item-action list-group-item-light text_white hover_black" onclick=loadData(' + categories.id + ') >' + categories.id + ".- " + categories.nombre +
                    '</li>' + '<i class="fa fa-trash category" onclick="deleteCategory(' + categories.id + ')"></i>' + '</div>'
            });

        });

}

function loadAllData() {

    fetch(BASE_URL + '/UsersData/')
        .then(response => response.json())
        .then(data => {
            clearTable()
            data.forEach(element => {

                document.getElementById("tableData").innerHTML +=
                    '<tr class="border border-dark align-middle">' +
                    '<th scope="row" class="border border-dark align-middle text-center">' + element.id + '</th>' +
                    '<td class="border border-dark align-middle text-center">' + element.site + '</td>' +
                    '<td class="border border-dark align-middle text-center">' + element.user + '</td>' +
                    '<td style="cursor:pointer;" id=' + 'pass-' + element.id + " " + 'class="border border-dark align-middle text-center" onclick=viewPassword(' + element.id + ')>' + '*******' + '</td>' +
                    '<td class="d-flex justify-content-around align-middle"><i class="fas fa-external-link-alt p-2" style="cursor:pointer;" onclick="openSite(' + element.id + ')""></i><i class="fas fa-pencil-alt p-2" style="cursor:pointer;" onclick="editSite(' + element.id + ')""></i><i class="fas fa-trash p-2" style="cursor:pointer;" onclick="deleteSite(' + element.id + ')""></i></span>' +
                    '</td>' +
                    '</tr>'
            });

        }).then(clearCategories());

}



function loadSecureCategoryData(id) {
    fetch(BASE_URL + '/UsersData/Category/' + id)
        .then(response => response.json())
        .then(data => {
            clearTable()
            data.forEach(element => {

                document.getElementById("tableData").innerHTML +=
                    '<tr class="border border-dark align-middle">' +
                    '<th scope="row" class="border border-dark align-middle text-center">' + element.id + '</th>' +
                    '<td class="border border-dark align-middle text-center" >' + element.site + '</td>' +
                    '<td class="border border-dark align-middle text-center" >' + element.user + '</td>' +
                    '<td class="border border-dark align-middle text-center" style="cursor:pointer;" onclick=viewPassword(' + element.id + ')>' + '*******' + '</td>' +
                    '<td class="d-flex justify-content-around align-middle"><i class="fas fa-external-link-alt p-2" style="cursor:pointer;" onclick="openSite(' + element.id + ')""></i><i class="fas fa-pencil-alt p-2" style="cursor:pointer;" onclick="editSite(' + element.id + ')""></i><i class="fas fa-trash p-2" style="cursor:pointer;" onclick="deleteSite(' + element.id + ')"></i></span>' +
                    '</td>' +
                    '</tr>'
            });
        });
}

function clearTable() {
    document.getElementById("tableData").innerHTML = ""
}

function loadData(id) {
    clearTable()
    clearCategories()
    document.getElementById('cat-' + id).classList.toggle("activated")
    loadSecureCategoryData(id)
}

function clearCategories() {
    let length = document.getElementsByClassName("list-group-item list-group-item-action list-group-item-light text_white hover_black").length
    for (let id = 1; id <= length; id++) {
        document.getElementById('cat-' + id).className = "list-group-item list-group-item-action list-group-item-light text_white hover_black"
    }
}

function loadPassword(id_site) {
    fetch(BASE_URL + '/UsersData/')
        .then(response => response.json())
        .then(data => {
            clearTable()
            data.forEach(element => {
                if (element.id == id_site) {
                    document.getElementById("tableData").innerHTML +=
                        '<tr class="border border-dark align-middle">' +
                        '<th scope="row" class="border border-dark align-middle text-center">' + element.id + '</th>' +
                        '<td class="border border-dark align-middle text-center" >' + element.site + '</td>' +
                        '<td class="border border-dark align-middle text-center" >' + element.user + '</td>' +
                        '<td class="border border-dark align-middle text-center" style="cursor:pointer; onclick=copy()">' + element.password + '</td>' +
                        '<td class="d-flex justify-content-around align-middle"><i class="fas fa-external-link-alt p-2" style="cursor:pointer;" onclick="openSite(' + element.id + ')""></i><i class="fas fa-pencil-alt p-2" style="cursor:pointer;" onclick="editSite(' + element.id + ')""></i><i class="fas fa-trash p-2" style="cursor:pointer;" onclick="deleteSite(' + element.id + ')"></i></span>' +
                        '</td>' +
                        '</tr>'
                } else {
                    document.getElementById("tableData").innerHTML +=
                        '<tr class="border border-dark align-middle">' +
                        '<th scope="row" class="border border-dark align-middle text-center">' + element.id + '</th>' +
                        '<td class="border border-dark align-middle text-center">' + element.site + '</td>' +
                        '<td class="border border-dark align-middle text-center">' + element.user + '</td>' +
                        '<td style="cursor:pointer;" id=' + 'pass-' + element.id + " " + 'class="border border-dark align-middle text-center" onclick=viewPassword(' + element.id + ')>' + '*******' + '</td>' +
                        '<td class="d-flex justify-content-around align-middle"><i class="fas fa-external-link-alt p-2" style="cursor:pointer;" onclick="openSite(' + element.id + ')""></i><i class="fas fa-pencil-alt p-2" style="cursor:pointer;" onclick="editSite(' + element.id + ')""></i><i class="fas fa-trash p-2" style="cursor:pointer;" onclick="deleteSite(' + element.id + ')""></i></span>' +
                        '</td>' +
                        '</tr>'
                }
            })
        });
}

function viewPassword(idPass) {
    let masterPass
    let count = 0
    do {
        masterPass = prompt("Master Key")
        if (masterPass == null) {
            break
        }
        if (masterPass == "123") {

            loadPassword(idPass)

            break
        }
        alert("Not correct key")
        count++;
        alert(`Attemp: ${count} / 3`)

    } while (count < 3);
}

function editSite(id_site) {
    window.location.href = ("/sites.html?id=" + id_site)
}

function openSite(id) {
    console.log('jasidjpsoadjapsodopiasjd')
    let id_int = parseInt(id)
    fetch('https://jorgemypasssaver.azurewebsites.net/UsersData/' + id_int)
        .then(response => response.json())
        .then(data => {
            console.log('aaaaaaaaa');
            window.open(data.site);
        });
}