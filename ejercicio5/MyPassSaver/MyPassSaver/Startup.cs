using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using static Jorge.MyPassSaver.Controllers.UsersDataController;
using static Jorge.MyPassSaver.Controllers.CategoriesController;

namespace Jorge.MyPassSaver
{
    public class Startup
    {
        private readonly string _apiVersion = "v1.1";
        private void AddInitialData()
        {
            UserDataList.Add(new Model.UserData() { Id = 1, Site = "https://www.twitter.com", User="jorge" ,Password = "123abc", CategoryId = 1});
            UserDataList.Add(new Model.UserData() { Id = 2, Site = "https://www.facebook.com", User="jorge" , Password = "123abc", CategoryId = 1 });
            UserDataList.Add(new Model.UserData() { Id = 3, Site = "https://www.instagram.com", User = "jorge", Password = "123abc", CategoryId = 1 });
            UserDataList.Add(new Model.UserData() { Id = 4, Site = "https://www.linkedin.com", User = "jorge", Password = "123abc", CategoryId = 1 });
            UserDataList.Add(new Model.UserData() { Id = 5, Site = "https://www.heraldo.es", User = "jorge", Password = "123abc", CategoryId = 2 });
            UserDataList.Add(new Model.UserData() { Id = 6, Site = "https://www.elpais.com", User = "jorge", Password = "123abc", CategoryId = 2 });
            UserDataList.Add(new Model.UserData() { Id = 7, Site = "https://www.as.com", User = "jorge", Password = "123abc", CategoryId = 3 });
            UserDataList.Add(new Model.UserData() { Id = 8, Site = "https://www.marca.com", User = "jorge", Password = "123abc", CategoryId = 3 });

            categoriesList.Add(new Model.Category() { Id = 1, Nombre = "Social", Descripcion = "Redes sociales" });
            categoriesList.Add(new Model.Category() { Id = 2, Nombre = "Noticias", Descripcion = "Redes sociales" });
            categoriesList.Add(new Model.Category() { Id = 3, Nombre = "Deporte", Descripcion = "Redes sociales" });
           
        }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c => { c.EnableAnnotations(); });
            services.AddControllers();

            services.AddSwaggerGen(options =>
            {

                options.SwaggerDoc(_apiVersion, new Microsoft.OpenApi.Models.OpenApiInfo()
                {
                    Title = $"API MyPassSaver ({_apiVersion})",
                    Version = _apiVersion,
                    Description = "API Rest a Password Saver",
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                    {
                        Name = "Jorge Mu�oz",

                        Email = "a24786@svalero.com"
                    }
                });
            });
        }
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            AddInitialData();
            app.UseSwagger();
            app.UseSwaggerUI(o =>
            {
                o.SwaggerEndpoint($"/swagger/{_apiVersion}/swagger.json", _apiVersion);
                o.RoutePrefix = string.Empty;

            });
        }
    }
}
