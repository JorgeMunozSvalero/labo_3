﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jorge.MyPassSaver.Model
{
    public class UserData
    {
        public int Id { get; set; }
        public string Site { get; set; }
        public string User{ get; set; }
        public string Password { get; set; }
        public int CategoryId { get; set; }

    }
}
