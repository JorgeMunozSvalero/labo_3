﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Jorge.MyPassSaver.Model;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Jorge.MyPassSaver.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        public static List<Category> categoriesList = new List<Category>();

        private Category searchById(int id)
        { 
            return categoriesList.FirstOrDefault(cat => cat.Id == id);
        }

        // GET: api/<CategoriesController>
        [HttpGet]
        [SwaggerOperation(Summary = "Gets all categ.", Description = "Gets all categories information")]
        public IEnumerable<Category> Get()
        {
            return categoriesList;
        }

        // GET api/<CategoriestController>/5
        [HttpGet("{id}", Name = "GetCat")]
        [SwaggerOperation(Summary = "Gets categ. by id", Description = "Gets all info from ID categ")]
        public Category Get(int id)
        {
            return categoriesList.FirstOrDefault(p => p.Id == id);
        }

        // POST api/<CategoriesController>
        [HttpPost]
        [SwaggerOperation(Summary = "Adds a new categ", Description = "You can add a new category passing all the required values")]
        public void Categories([FromBody] Category value)
        {
            if (categoriesList.Any())
                value.Id = categoriesList.Max(p => p.Id) + 1;
            else
                value.Id = 1;
            categoriesList.Add(value);
        }

        // PUT api/<CategoriesController>/5
        [HttpPut("{id}")]
        [SwaggerOperation(Summary = "Updates a categ", Description = "You can update a category identified by the id")]
        public void Put(int id, [FromBody] Category value)
        {
            var catToUpdate = categoriesList.FirstOrDefault(p => p.Id == id);
            if (catToUpdate == null)
                return;
            categoriesList.Remove(catToUpdate);
            categoriesList.Add(value);
        }

        // DELETE api/<CategoriesController>/5
        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Deletes a categ", Description = "You can delete a category identified by the id")]
        
        public void Delete(int id)
        {
            var catToDelete = categoriesList.FirstOrDefault(p => p.Id == id);
            if (catToDelete == null)
                return;
            categoriesList.Remove(catToDelete);
        }
    }
}
