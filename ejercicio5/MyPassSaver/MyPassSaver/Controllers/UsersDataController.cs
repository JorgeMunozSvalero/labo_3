﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Jorge.MyPassSaver.Model;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Jorge.MyPassSaver.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersDataController : ControllerBase
    {

        public static List<UserData> UserDataList = new List<UserData>();
        private readonly ILogger _logger;

        public UsersDataController(ILogger<UsersDataController> logger)
        {
            _logger = logger;
        }

        // GET: api/<UsersDataController>
        [HttpGet]
        [SwaggerOperation(Summary = "Gets all user data.", Description = "Gets all user information")]
        public IEnumerable<UserData> Get()
        {
            _logger.LogInformation("Get user data");
            return UserDataList;
        }

        // GET api/<UsersDataController>/5
        [HttpGet("{id}", Name = "Get")]
        [SwaggerOperation(Summary = "Gets user data by the id", Description = "Gets all info from that user information with the id")]
        public UserData Get(int id)
        {
            return UserDataList.FirstOrDefault(p => p.Id == id);
        }

        // GET api/<UsersDataController>/Category/
        [HttpGet("Category/{id_Cat}", Name = "GetDataByCategory")]
        [SwaggerOperation(Summary = "Gets a site by the category", Description = "You can get a site by the category id")]
        public IEnumerable<UserData> GetByCategory(int id_Cat)
        {
          
            return UserDataList.Where(u => u.CategoryId == id_Cat);
  
        }

        // POST api/<UsersDataController>
        [HttpPost]
        [SwaggerOperation(Summary = "Adds a new site", Description = "You can add a new site passing all the required values")]
        public void UserData([FromBody] UserData value)
        {
            if (UserDataList.Any())
            {
                value.Id = UserDataList.Max(p => p.Id) + 1;
                UserDataList.Add(value);
            }
            else
            {
                value.Id = 1;
                UserDataList.Add(value);
            }
                
        }

        // PUT api/<UsersDataController>/5
        [HttpPut("{id}")]
        [SwaggerOperation(Summary = "Update a site", Description = "You can update a site by the id")]
        public void Put(int id, [FromBody] UserData value)
        {
            var elementToUpdate = UserDataList.FirstOrDefault(p => p.Id == id);
            if (elementToUpdate == null)
                return;
            UserDataList.Remove(elementToUpdate);
            UserDataList.Add(value);
        }

        // DELETE api/<UsersDataController>/5
        [HttpDelete("{id}")]
        [SwaggerOperation(Summary = "Delete a site", Description = "You can delete a site by the id")]
        public void Delete(int id)
        {
            var elementToDelete = UserDataList.FirstOrDefault(p => p.Id == id);
            if (elementToDelete == null)
                return;
            UserDataList.Remove(elementToDelete);
        }
    }
}
